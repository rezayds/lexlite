<?php include 'partial/header.php'; ?>
		<div id="section-one" class="full-height prim-color">
			<div class="container py-5">
				<div class="row">
					<div class="col-md-6 my-5">
						<h1 class="display-4 acc-color-text headline">Best Headline in Town!</h1>
						<p class="lead white-text description">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
						</p>
						<a class="btn-hover">Look Forward</a>
					</div>
					<div class="col-md-6">
						<div class="x-img"></div>
					</div>
				</div>
			</div>
		</div>

		<div id="section-two" class="prim-color">
			<div class="container py-5 text-center">
				<h5 class="small-text">Hello There</h5>
				<h1 class="display-4 white-text headline-text">We Are LexLite</h1>
				<p class="lead white-text">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</p>
			</div>
		</div>

		<div id="section-three" class="acc-color py-5">
			<h5 class="small-text text-center">Let's See</h5>
			<h1 class="display-4 white-text text-center">What We Do</h1>
			<div class="container py-4">
				<div class="row">
					<div class="col-md-4 py-3">
						<div class="card card-shadow mx-3">
							<i class="fas fa-balance-scale display-4 text-center acc-color-text"></i>
							<div class="card-body text-center">
								<h5 class="card-title">Card title</h5>
						    		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
							</div>
						</div>
					</div>
					<div class="col-md-4 py-3">
						<div class="card card-shadow mx-3">
							<i class="fas fa-balance-scale display-4 text-center acc-color-text"></i>
							<div class="card-body text-center">
								<h5 class="card-title">Card title</h5>
						    		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
							</div>
						</div>
					</div>
					<div class="col-md-4 py-3">
						<div class="card card-shadow mx-3">
							<i class="fas fa-balance-scale display-4 text-center acc-color-text"></i>
							<div class="card-body text-center">
								<h5 class="card-title">Card title</h5>
						    		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
							</div>
						</div>
					</div>
				</div>
				<div class="row py-5">
					<div class="col-md-4 py-3">
						<div class="card card-shadow mx-3">
							<i class="fas fa-balance-scale display-4 text-center acc-color-text"></i>
							<div class="card-body text-center">
								<h5 class="card-title">Card title</h5>
						    		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
							</div>
						</div>
					</div>
					<div class="col-md-4 py-3">
						<div class="card card-shadow mx-3">
							<i class="fas fa-balance-scale display-4 text-center acc-color-text"></i>
							<div class="card-body text-center">
								<h5 class="card-title">Card title</h5>
						    		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
							</div>
						</div>
					</div>
					<div class="col-md-4 py-3">
						<div class="card card-shadow mx-3">
							<i class="fas fa-balance-scale display-4 text-center acc-color-text"></i>
							<div class="card-body text-center">
								<h5 class="card-title">Card title</h5>
						    		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php include 'partial/footer.php'; ?>