<?php include 'partial/header.php'; ?>
<div id="about-us-1" class="prim-color">
	<div class="container py-5">
		<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-about-us" role="tab" aria-controls="pills-home" aria-selected="true">About Us</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-our-mission" role="tab" aria-controls="pills-profile" aria-selected="false">Our Mission</a>
			</li>
		</ul>
		<div class="tab-content" id="pills-tabContent">
			<div class="tab-pane fade show active" id="pills-about-us" role="tabpanel" aria-labelledby="pills-home-tab">
				<div class="row">
					<div class="col-md-4 pt-3">
						<div class="logo"></div>
					</div>
					<div class="col-md-8 pt-3 desc-text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="pills-our-mission" role="tabpanel" aria-labelledby="pills-profile-tab">
				<div class="row">
					<div class="col-md-4 pt-3">
						<div class="logo"></div>
					</div>
					<div class="col-md-8 pt-3 desc-text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="team" class="acc-color">
	<div class="container py-5">
		<h1 class="display-4 white-text text-center">Awesome Team</h1>
		<div class="row">

			<div class="col-md-3 my-4">
				<div class="image-flip" >
					<div class="mainflip flip-0">
						<div class="frontside">
							<div class="card">
								<div class="card-body text-center">
									<p><img class=" img-fluid" src="https://sunlimetech.com/portfolio/boot4menu/assets/imgs/team/img_01.png" alt="card image"></p>
									<h4 class="card-title">Your Name</h4>
									<p class="card-text">This is basic information about you</p>
									<a href="https://www.fiverr.com/share/qb8D02" class="btn acc-color white-text btn-sm"><i class="fa fa-plus"></i></a>
								</div>
							</div>
						</div>
						<div class="backside">
							<div class="card">
								<div class="card-body text-center mt-4">
									<h4 class="card-title">Your Name</h4>
									<p class="card-text">This is some detail information about you, such as your interest, what you do, skills, and anything.</p>
									<ul class="list-inline">
										<li class="list-inline-item">
											<a class="social-icon text-xs-center" target="_blank" href="#">
												<i class="fa fa-facebook acc-color-text"></i>
											</a>
										</li>
										<li class="list-inline-item">
											<a class="social-icon text-xs-center" target="_blank" href="#">
												<i class="fa fa-twitter acc-color-text"></i>
											</a>
										</li>
										<li class="list-inline-item">
											<a class="social-icon text-xs-center" target="_blank" href="#">
												<i class="fa fa-instagram acc-color-text"></i>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3 my-4">
				<div class="image-flip" >
					<div class="mainflip flip-0">
						<div class="frontside">
							<div class="card">
								<div class="card-body text-center">
									<p><img class=" img-fluid" src="https://sunlimetech.com/portfolio/boot4menu/assets/imgs/team/img_01.png" alt="card image"></p>
									<h4 class="card-title">Your Name</h4>
									<p class="card-text">This is basic information about you</p>
									<a href="https://www.fiverr.com/share/qb8D02" class="btn acc-color white-text btn-sm"><i class="fa fa-plus"></i></a>
								</div>
							</div>
						</div>
						<div class="backside">
							<div class="card">
								<div class="card-body text-center mt-4">
									<h4 class="card-title">Your Name</h4>
									<p class="card-text">This is some detail information about you, such as your interest, what you do, skills, and anything.</p>
									<ul class="list-inline">
										<li class="list-inline-item">
											<a class="social-icon text-xs-center" target="_blank" href="#">
												<i class="fa fa-facebook acc-color-text"></i>
											</a>
										</li>
										<li class="list-inline-item">
											<a class="social-icon text-xs-center" target="_blank" href="#">
												<i class="fa fa-twitter acc-color-text"></i>
											</a>
										</li>
										<li class="list-inline-item">
											<a class="social-icon text-xs-center" target="_blank" href="#">
												<i class="fa fa-instagram acc-color-text"></i>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3 my-4">
				<div class="image-flip" >
					<div class="mainflip flip-0">
						<div class="frontside">
							<div class="card">
								<div class="card-body text-center">
									<p><img class=" img-fluid" src="https://sunlimetech.com/portfolio/boot4menu/assets/imgs/team/img_01.png" alt="card image"></p>
									<h4 class="card-title">Your Name</h4>
									<p class="card-text">This is basic information about you</p>
									<a href="https://www.fiverr.com/share/qb8D02" class="btn acc-color white-text btn-sm"><i class="fa fa-plus"></i></a>
								</div>
							</div>
						</div>
						<div class="backside">
							<div class="card">
								<div class="card-body text-center mt-4">
									<h4 class="card-title">Your Name</h4>
									<p class="card-text">This is some detail information about you, such as your interest, what you do, skills, and anything.</p>
									<ul class="list-inline">
										<li class="list-inline-item">
											<a class="social-icon text-xs-center" target="_blank" href="#">
												<i class="fa fa-facebook acc-color-text"></i>
											</a>
										</li>
										<li class="list-inline-item">
											<a class="social-icon text-xs-center" target="_blank" href="#">
												<i class="fa fa-twitter acc-color-text"></i>
											</a>
										</li>
										<li class="list-inline-item">
											<a class="social-icon text-xs-center" target="_blank" href="#">
												<i class="fa fa-instagram acc-color-text"></i>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3 my-4">
				<div class="image-flip" >
					<div class="mainflip flip-0">
						<div class="frontside">
							<div class="card">
								<div class="card-body text-center">
									<p><img class=" img-fluid" src="https://sunlimetech.com/portfolio/boot4menu/assets/imgs/team/img_01.png" alt="card image"></p>
									<h4 class="card-title">Your Name</h4>
									<p class="card-text">This is basic information about you</p>
									<a href="https://www.fiverr.com/share/qb8D02" class="btn acc-color white-text btn-sm"><i class="fa fa-plus"></i></a>
								</div>
							</div>
						</div>
						<div class="backside">
							<div class="card">
								<div class="card-body text-center mt-4">
									<h4 class="card-title">Your Name</h4>
									<p class="card-text">This is some detail information about you, such as your interest, what you do, skills, and anything.</p>
									<ul class="list-inline">
										<li class="list-inline-item">
											<a class="social-icon text-xs-center" target="_blank" href="#">
												<i class="fa fa-facebook acc-color-text"></i>
											</a>
										</li>
										<li class="list-inline-item">
											<a class="social-icon text-xs-center" target="_blank" href="#">
												<i class="fa fa-twitter acc-color-text"></i>
											</a>
										</li>
										<li class="list-inline-item">
											<a class="social-icon text-xs-center" target="_blank" href="#">
												<i class="fa fa-instagram acc-color-text"></i>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>



		</div>
	</div>
</div>


<?php include 'partial/footer.php'; ?>