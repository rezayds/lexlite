<!doctype html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="assets/css/material-color.css">
		<link rel="stylesheet" type="text/css" href="assets/css/style.css">
		<title>LexLite</title>
	</head>
	<body>

		<nav id="navbar-shadow" class="navbar navbar-expand-sm navbar-dark sticky-top py-md-4 prim-color">
			<div class="container">
				<a class="navbar-brand acc-color-text" href="index.php"><b>LexLite</b></a>
				<button id="custom-toggler" class="navbar-toggler" 
					type="button" 
					data-toggle="collapse" 
					data-target="#navbarSupportedContent" 
					aria-controls="navbarSupportedContent" 
					aria-expanded="false" 
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item px-md-4">
							<a class="nav-link" href="#">Edu Section</a>
						</li>
						<li class="nav-item px-md-4">
							<a class="nav-link" href="#">Contribute</a>
						</li>
						<li class="nav-item px-md-4">
							<a class="nav-link" href="#">Request Content</a>
						</li>
						<li class="nav-item px-md-4">
							<a class="nav-link" href="about-us.php">About Us</a>
						</li>
						<li class="nav-item px-md-4">
							<a class="nav-link" href="#">Support Us</a>
						</li>
						<!-- <li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Dropdown
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="#">Action</a>
								<a class="dropdown-item" href="#">Another action</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#">Something else here</a>
							</div>
						</li> -->
					</ul>
					<form class="form-inline my-2 my-lg-0">
						<div class="d-sm-none">
							<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
							<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
						</div>
						
						<div id="search-icon" class="d-none d-sm-block">
							<input type="search" placeholder="Search">	
						</div>
					</form>
				</div>
			</div>			
		</nav>