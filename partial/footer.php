		<!-- <div id="footer">

			<div class="container py-3">
				<span class="text-muted">Copyright 2020 Lexlite</span>
			</div>
		</div> -->

		<footer class="container py-5">
			<div class="row">
				<div class="col-12 col-md">
					LexLite
					<small class="d-block mb-3 text-muted">&copy; 2017-2018</small>
				</div>
				<div class="col-6 col-md">
					<h5>About</h5>
					<ul class="list-unstyled text-small">
						<li><a class="text-muted" href="#">Team</a></li>
			            <li><a class="text-muted" href="#">Locations</a></li>
			            <li><a class="text-muted" href="#">Privacy</a></li>
			            <li><a class="text-muted" href="#">Terms</a></li>
					</ul>
				</div>
				<div class="col-6 col-md">
					<h5>Features</h5>
					<ul class="list-unstyled text-small">
			            <li><a class="text-muted" href="#">Cool stuff</a></li>
			            <li><a class="text-muted" href="#">Random feature</a></li>
			            <li><a class="text-muted" href="#">Team feature</a></li>
			            <li><a class="text-muted" href="#">Stuff for developers</a></li>
			            <li><a class="text-muted" href="#">Another one</a></li>
			            <li><a class="text-muted" href="#">Last time</a></li>
          			</ul>
        		</div>
		        <div class="col-6 col-md">
		         	<h5>Resources</h5>
		         	<ul class="list-unstyled text-small">
		            	<li><a class="text-muted" href="#">Resource</a></li>
		            	<li><a class="text-muted" href="#">Resource name</a></li>
		            	<li><a class="text-muted" href="#">Another resource</a></li>
		            	<li><a class="text-muted" href="#">Final resource</a></li>
		          	</ul>
		        </div>
			</div>

		</footer>


		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
		<script src="https://kit.fontawesome.com/6972b7bfd3.js" crossorigin="anonymous"></script>
		<script src="assets/js/style.js"></script>
	</body>
</html>